import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import registerUser from "./controllers/registerUser";
import loginUser from "./controllers/loginUser";
import getMyInfo from "./controllers/getMyInfo";
import deleteMyInfo from "./controllers/deleteMyInfo";
import updateMyPassword from "./controllers/updateMyPassword";
import addNote from "./controllers/addNote";
import getMyNotes from "./controllers/getMyNotes";
import findNote from "./controllers/findNote";
import updateNote from "./controllers/updateNote";
import toggleNote from "./controllers/toggleNote";
import deleteNote from "./controllers/deleteNote";

require("dotenv").config();

const URI = `mongodb+srv://${process.env.dbLOGIN}:${process.env.dbPASSWORD}@assignment2.jc6za.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;
mongoose.connect(
  URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    autoIndex: true,
  },
  (err) => {
    if (err) {
      throw new Error(err);
    }
    console.log("connected to mongoDB");
  }
);

const app = express();
const PORT = process.env.PORT || 8080;

app.use(express.json());
app.use(express.static("./build"));
app.use(cors());
app.use((req, res, next) => {
  console.log(req.method, req.originalUrl);
  next();
});

app.get("/", (req, res) => {
  res
    .status(200)
    .send("<h1 style='text-align:center'>Node.js Assignment 1</h1>");
});

app
  .route("/api/users/me")
  .get(getMyInfo)
  .delete(deleteMyInfo)
  .patch(updateMyPassword);

app.route("/api/notes").get(getMyNotes).post(addNote);

app
  .route("/api/notes/:id")
  .get(findNote)
  .put(updateNote)
  .patch(toggleNote)
  .delete(deleteNote);

app.post("/api/auth/register", registerUser);

app.post("/api/auth/login", loginUser);

app.listen(PORT, () => {
  console.log(`Starting the server on port ${PORT}`);
});
