import mongoose from "mongoose";

const UserSchema = mongoose.Schema({
  username: String,
  password: String,
  createdDate: Date,
});

export default mongoose.model("UserModel", UserSchema);
