import mongoose from "mongoose";

const NoteSchema = mongoose.Schema({
  userId: String,
  completed: Boolean,
  text: String,
  createdDate: Date,
});

export default mongoose.model("NoteModel", NoteSchema);
