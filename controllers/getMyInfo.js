import { ERROR_MESSAGE } from "../utils/constants";
import jwt from "jsonwebtoken";

const getMyInfo = (req, res) => {
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    res.status(200).json({
      user: {
        _id: myData._id,
        username: myData.username,
        createdDate: myData.createdDate,
      },
    });
  }
};

export default getMyInfo;
