import { ERROR_MESSAGE, SUCCESS_MESSAGE } from "../utils/constants";
import NoteModel from "../models/NoteModel";

const updateNote = (req, res) => {
  const { text } = req.body;
  const { id } = req.params;
  const [, token] = req.headers["authorization"].split(" ");
  if (!text || !token) {
    res.status(400).json(ERROR_MESSAGE);
  } else {
    NoteModel.findOneAndUpdate(
      {
        _id: id,
      },
      { text: text },
      { new: true },
      (err, note) => {
        if (err) {
          res.status(500).json(ERROR_MESSAGE);
        } else {
          if (!note) {
            res.status(400).json(ERROR_MESSAGE);
          } else {
            res.status(200).json(SUCCESS_MESSAGE);
          }
        }
      }
    );
  }
};

export default updateNote;
