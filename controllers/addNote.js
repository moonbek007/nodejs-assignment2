import { ERROR_MESSAGE, SUCCESS_MESSAGE } from "../utils/constants";
import jwt from "jsonwebtoken";
import NoteModel from "../models/NoteModel";

const addNote = (req, res) => {
  const [, token] = req.headers["authorization"].split(" ");
  const { text } = req.body;
  if (!text || !token) {
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    NoteModel.create(
      {
        userId: myData._id,
        completed: false,
        text,
        createdDate: new Date(),
      },
      (err, note) => {
        if (err) {
          res.status(500).json(ERROR_MESSAGE);
        } else {
          res.status(200).json(SUCCESS_MESSAGE);
        }
      }
    );
  }
};

export default addNote;
