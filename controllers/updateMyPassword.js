import { ERROR_MESSAGE, SUCCESS_MESSAGE } from "../utils/constants";
import jwt from "jsonwebtoken";
import UserModel from "../models/UserModel";
import bcrypt from "bcrypt";

const updateMyPassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const [, token] = req.headers["authorization"].split(" ");
  if (!oldPassword || !newPassword || !token) {
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    const newPasswordHashed = await bcrypt.hash(newPassword, 10);
    UserModel.findOneAndUpdate(
      { username: myData.username },
      { password: newPasswordHashed },
      { new: true },
      (err, user) => {
        if (err) res.status(500).json(ERROR_MESSAGE);
        else {
          res.status(200).json(SUCCESS_MESSAGE);
        }
      }
    );
  }
};

export default updateMyPassword;
