import { ERROR_MESSAGE, SUCCESS_MESSAGE } from "../utils/constants";
import jwt from "jsonwebtoken";
import UserModel from "../models/UserModel";

const deleteMyInfo = (req, res) => {
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    UserModel.deleteOne({ username: myData.username }, (err) => {
      if (err) res.status(500).json(ERROR_MESSAGE);
      else {
        res.status(200).json({
          ...SUCCESS_MESSAGE,
        });
      }
    });
  }
};

export default deleteMyInfo;
