import UserModel from "../models/UserModel";
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from "../utils/constants";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

const loginUser = (req, res) => {
  const { username, password } = req.body;
  UserModel.findOne({ username }, async (err, user) => {
    if (err) {
      res.status(500).json(ERROR_MESSAGE);
    } else {
      if (user == null || !(await bcrypt.compare(password, user.password))) {
        res.status(400).json(ERROR_MESSAGE);
      } else {
        const token = jwt.sign(
          { username, _id: user._id, createdDate: user.createdDate },
          "secret"
        );
        res.status(200).json({ ...SUCCESS_MESSAGE, jwt_token: token });
      }
    }
  });
};

export default loginUser;
