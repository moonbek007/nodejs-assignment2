import { ERROR_MESSAGE } from "../utils/constants";
import jwt from "jsonwebtoken";
import NoteModel from "../models/NoteModel";

const getMyNotes = (req, res) => {
  const [, token] = req.headers["authorization"].split(" ");
  const offset = req.query.offset || 0;
  const limit = req.query.limit || 0;
  if (!token || offset < 0 || limit < 0) {
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    NoteModel.find({ userId: myData._id }, (err, notes) => {
      if (err) res.status(500).json(ERROR_MESSAGE);
      else {
        const results =
          limit > 0 ? notes.slice(offset).slice(0, limit) : notes.slice(offset);
        res.status(200).json({
          offset,
          limit,
          count: notes.length,
          notes: results,
        });
      }
    });
  }
};

export default getMyNotes;
