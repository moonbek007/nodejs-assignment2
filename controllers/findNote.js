import { ERROR_MESSAGE, SUCCESS_MESSAGE } from "../utils/constants";
import NoteModel from "../models/NoteModel";

const findNote = async (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers["authorization"].split(" ");
  if (!id || !token) {
    res.status(400).json(ERROR_MESSAGE);
  } else {
    NoteModel.findOne(
      {
        _id: id,
      },
      (err, note) => {
        if (err) {
          res.status(500).json(ERROR_MESSAGE);
        } else {
          if (!note) {
            res.status(400).json(ERROR_MESSAGE);
          } else {
            const { _id, userId, completed, text, createdDate } = note;
            res
              .status(200)
              .json({ note: { _id, userId, completed, text, createdDate } });
          }
        }
      }
    );
  }
};

export default findNote;
