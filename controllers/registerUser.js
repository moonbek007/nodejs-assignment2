import UserModel from "../models/UserModel";
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from "../utils/constants";
import bcrypt from "bcrypt";

const registerUser = (req, res) => {
  const { username, password } = req.body;
  UserModel.findOne({ username }, async (err, user) => {
    if (err) {
      res.status(500).json(ERROR_MESSAGE);
    } else {
      if (user == null) {
        UserModel.create(
          {
            username,
            password: await bcrypt.hash(password, 10),
            createdDate: new Date(),
          },
          (err, user) => {
            if (err) {
              res.status(500).json(ERROR_MESSAGE);
            } else {
              res.status(200).json(SUCCESS_MESSAGE);
            }
          }
        );
      } else {
        res.status(400).json(ERROR_MESSAGE);
      }
    }
  });
};

export default registerUser;
